#!/usr/bin/env python

import urllib2
import sys
from time import sleep
import argparse
import os, os.path
import difflib
import logging

def logger(message):
    """Setup logging to web_monitor.log"""
    logging.basicConfig(filename='web_monitor.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    # use isinstance to determine if message is a list
    if isinstance(message, list):
        # log each line of message and strip out any newlines
        [logging.warning(x.strip()) for x in message]
    else:
        logging.warning(message)

def get_site(url, tries):
    """Try to load the site x number of time."""
    attempts = 0
   
    while attempts < tries:
        try:
            request = urllib2.Request(url)
            response = urllib2.urlopen(request)
            return response.readlines()
        except:
            attempts += 1
            print "Connection failed, trying again."
            sleep(2)
    sys.exit(1)

def store_site(path, data):
    """Used to generate site.orig."""
    with open(path, 'wb') as output:
        for line in data:
            output.write(line)
        output.close()

def load_original(path):
    """Load site.orig."""
    with open(path, 'r') as input:
        data= input.readlines()
        input.close()

    return data

def compare(file, site, tries):
    """Compare site.orig with current running site."""
    orig = load_original(file)
    new_site = get_site(site, tries)
    diff = difflib.unified_diff(
        orig, new_site,
        fromfile='orig',
        tofile='live_site',
        n=3
    )

    try:
        # Check generator for any values and set first_item
        first_item = diff.next()
    except StopIteration:
        print "unchanged"
        logger("unchanged")
        first_item = None

    if first_item is not None:
        print "changed"
        logger("changed")
        output = list(diff)

        for line in output:
            line = line.rstrip('\n')
            print line
        
        logger(output)
        sys.exit(1)

def create_files(url, tries, site_file):
    data = get_site(url, tries)
    store_site(site_file, data)

def main():
    """And let the magic begin."""

    site_file = "site.orig"
    
    # Parse command line args
    parser = argparse.ArgumentParser()
    parser.add_argument("--retry", help="How many times to retry. Default: 2", type=int, action="store", default=2)
    parser.add_argument("--url", help="Url to run a compare against. Default: http://www.oracle.com/index.html",
            action="store", default="http://www.oracle.com/index.html")
    parser.add_argument("--refresh", help="Refresh stored site.", action="store_true")
    args = parser.parse_args()
  
    tries = args.retry
    url = args.url
    refresh = args.refresh

    # Create site file if it does not exist.
    # Refresh/regenerate incase site change is correct.
    if not os.path.exists(site_file):
        print "First run or site.orig is missing. Run again to compare."
        create_files(url, tries, site_file)
        sys.exit(0)
    if refresh:
        print "Refreshing site.orig with current site content."
        create_files(url, tries, site_file)
        sys.exit(0)

    # Do the actual comparison between site.orig and live site.    
    compare(site_file, url, tries)

if __name__ == '__main__':
    main()
